<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
  'register' => false, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
]);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin' , 'middleware' => 'authadmin'], function () {
  Route::get('/dashboard','AdminController@index');
  Route::get('/pageGps','AdminController@pageGps');
  Route::get('/pageJual','AdminController@pageJual');
  Route::get('/pageUser','AdminController@pageUser');
  
  // Table
  Route::get('/getTableList','DataTableController@getTableList');
  Route::get('/getTableListJual','DataTableController@getTableListJual');
  Route::get('/getTableUser','DataTableController@getTableUser');

  // Post
  Route::post('/postGps','AdminController@postGps');
  Route::post('/editGps','AdminController@editGps');
  Route::get('/hapusGps/{id}','AdminController@hapusGps');

  // post jual_kepada
  Route::post('/postJual','AdminController@postJual');
  Route::post('/updateJual','AdminController@updateJual');
  Route::get('/deleteJual/{id}','AdminController@deleteJual');

  // post user by admin
  Route::post('/tambahUser','AuthController@tambahUser');
  Route::post('/editUser','AuthController@editUser');
  Route::post('/editPassword','AuthController@editPassword');
  Route::get('/deleteUser/{id}','AuthController@deleteUser');
});

Route::group(['prefix' => 'user', 'middleware' =>'authuser'], function () {
  Route::get('/dashboard','UserController@index');
  Route::get('/pageUser','UserController@pageUser');
  Route::get('/pageGps','UserController@pageGps');
  Route::get('/pageJual','UserController@pageJual');

  Route::post('/userUpdate','AuthController@userUpdate');
  Route::post('/userPassword','AuthController@userPassword');

  
  // Table
  Route::get('/getTableList','DataTableController@getTableList');
  Route::get('/getTableListJual','DataTableController@getTableListJual');
  Route::get('/getTableUser','DataTableController@getTableUser');

  // Post
  Route::post('/postGps','UserController@postGps');
  Route::post('/editGps','UserController@editGps');
  Route::get('/hapusGps/{id}','UserController@hapusGps');

  // post jual_kepada
  Route::post('/postJual','UserController@postJual');
  Route::post('/updateJual','UserController@updateJual');
  Route::get('/deleteJual/{id}','UserController@deleteJual');
});


