-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for gpsdb
CREATE DATABASE IF NOT EXISTS `gpsdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gpsdb`;

-- Dumping structure for table gpsdb.datagps
CREATE TABLE IF NOT EXISTS `datagps` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `brand_gps` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_gps` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_gps` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `garansi_gps` int(11) NOT NULL,
  `tgl_beli_gps` date NOT NULL,
  `photo_gps` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desciption_gps` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gpsdb.datagps: ~0 rows (approximately)
/*!40000 ALTER TABLE `datagps` DISABLE KEYS */;
INSERT INTO `datagps` (`id`, `brand_gps`, `model_gps`, `nama_gps`, `garansi_gps`, `tgl_beli_gps`, `photo_gps`, `desciption_gps`, `created_at`, `updated_at`) VALUES
	(1, 'Test', 'Bebas', 'Seri 1', 1, '2020-02-22', 'GPS/20-02-2020 18-31-46/Images/2020-02-20Images.jpg', 'test', '2020-02-20 18:31:46', '2020-02-20 18:31:46');
/*!40000 ALTER TABLE `datagps` ENABLE KEYS */;

-- Dumping structure for table gpsdb.datajuals
CREATE TABLE IF NOT EXISTS `datajuals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gps_id` bigint(20) NOT NULL,
  `tgl_jual_gps` date NOT NULL,
  `jual_kepada` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gpsdb.datajuals: ~1 rows (approximately)
/*!40000 ALTER TABLE `datajuals` DISABLE KEYS */;
INSERT INTO `datajuals` (`id`, `gps_id`, `tgl_jual_gps`, `jual_kepada`, `created_at`, `updated_at`) VALUES
	(1, 1, '2020-02-22', 'PT. Rental Test', '2020-02-20 18:32:09', '2020-02-20 18:32:09');
/*!40000 ALTER TABLE `datajuals` ENABLE KEYS */;

-- Dumping structure for table gpsdb.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gpsdb.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table gpsdb.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gpsdb.migrations: ~6 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2020_02_20_024156_create_data_g_p_s_s_table', 1),
	(5, '2020_02_20_031503_create_datagps_table', 2),
	(6, '2020_02_20_031933_create_datajuals_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table gpsdb.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gpsdb.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table gpsdb.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gpsdb.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@admin.com', NULL, '$2y$10$iTkdqQPNKbrWCEiqMm.tyu.pnCcia8sm4XINzx7b3wuMMV61lz.xO', 1, NULL, NULL, NULL),
	(2, 'user', 'user@user.com', NULL, '$2y$10$gIcX0n/wHFO7HbKcbjDtv.gTit6Vv6kTPBoPNNO6ZgxlWXInTs8Xe', 2, NULL, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
