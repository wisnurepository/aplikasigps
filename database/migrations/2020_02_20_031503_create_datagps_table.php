<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatagpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datagps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand_gps');
            $table->string('model_gps');
            $table->string('nama_gps');
            $table->integer('garansi_gps');
            $table->date('tgl_beli_gps');
            $table->string('photo_gps');
            $table->longText('desciption_gps');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datagps');
    }
}
