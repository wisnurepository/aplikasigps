<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'admin',
          'email' => 'admin@admin.com',
          'role' => 1,
          'password' => Hash::make('123123'),
      ]);
      
      DB::table('users')->insert([
        'name' => 'user',
        'email' => 'user@user.com',
        'role' => 2,
        'password' => Hash::make('123123'),
    ]);      
    }
}
