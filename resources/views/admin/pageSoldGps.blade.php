<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <title>Aplikasi Data GPS</title>
  </head>
  <body>

    <div class="container mt-5">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="/">Aplikasi Data GPS</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="/admin/dashboard">Dashboard <span class="sr-only">(current)</span></a>
          </li>          
          <li class="nav-item ">
            <a class="nav-link" href="/admin/pageUser">Manage User </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Manage GPS
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/admin/pageGps">GPS List</a>
              <a class="dropdown-item" href="/admin/pageJual">GPS Jual</a>
            </div>
          </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <a href="{{route('logout')}}" class="btn btn-danger"> Logout</a>
            {{-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
          </form>
        </div>
      </nav>
      <div class="jumbotron jumbotron-fluid p-0 mt-2">
        @if (session('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h5>List Penjualan
              <button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#tambahPenjual">Tambah Penjualan</button></h5>            
          </div>
          <div class="card-body">
            <div class="container">
              <table class="table" id="tableGps">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">id</th>
                    <th scope="col">idGps</th>
                    <th scope="col">Nama GPS</th>
                    <th scope="col">Tanggal Jual GPS</th>
                    <th scope="col">Nama Pembeli</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Modal Create Jual Here --}}
    <div class="modal fade bd-example-modal-lg" id="tambahPenjual" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" style="max-width:58%;">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Tambah Penjualan GPS</h5>
          </div>
          <div class="modal-body">
            <form method="post" action="/admin/postJual">
              @csrf
              <div class="form-row">
                <div class="col-4">
                  <select class="custom-select" name="idGps" required> 
                    <option selected>-- Nama Gps --</option>
                    @foreach($getGps as $item)
                      <option value="{{$item->id}}">{{$item->nama_gps}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-4">
                  <input type="date" class="form-control" name="tglJual" placeholder="" required>
                </div>
                <div class="col-4">
                  <input type="text" class="form-control" name="namaJual" placeholder="Nama Pembeli" required>
                </div>
              </div>
          </div>
          <div class="modal-footer">  
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-info btn-sm" > Tambah </button>
          </div>
        </form>
        </div>
      </div>
    </div>

    
    {{-- Modal Update Jual Here --}}
    <div class="modal fade bd-example-modal-lg" id="editJual" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" style="max-width:58%;">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Tambah Penjualan GPS</h5>
            <div class="form-group float-right">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="checkEdit">
                <label class="form-check-label" for="checkEdit">
                  Edit Data Jual GPS
                </label>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <form method="post" action="/admin/updateJual">
              @csrf
              <input type="hidden" name="idJual" id="idJual">
              <div class="form-row">
                <div class="col-4">
                  <select class="custom-select" name="idGpsEdit" id="idGpsEdit" required> 
                    <option selected>-- Nama Gps --</option>
                    @foreach($getGps as $item)
                      <option value="{{$item->id}}">{{$item->nama_gps}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-4">
                  <input type="date" class="form-control" name="tglJualEdit" id="tglJualEdit" placeholder="" required>
                </div>
                <div class="col-4">
                  <input type="text" class="form-control" name="namaJualEdit" id="namaJualEdit" placeholder="Nama Pembeli" required>
                </div>
              </div>
          </div>
          <div class="modal-footer">  
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-info btn-sm" id="btnEdit" > Tambah </button>
          </div>
        </form>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    

    <script>
      $(document).ready( function () {
      var table = $('#tableGps').DataTable(
          {
            "ordering": false,
            "info":     false,
            "bFilter": false,
            "bLengthChange": false,
            "processing": true,

            ajax : {
              url : "/admin/getTableListJual",
              type : "GET"
            },

            "columns" : [
              {'data':'no'},
              {'data':'idJual'},
              {'data':'idGps'},
              {'data':'namaGps'},
              {'data':'tglJualGps'},
              {'data':'jualKepada'},
            ],

            "columnDefs" : [{
            "targets": 0,
            class : 'text-center',
            // "visible" : false                
            },{
            "targets": 1,
            class : 'text-center',
            "visible" : false                
            },{
            "targets": 2,
            class : 'text-center',
            "visible" : false                
            },{
            "targets": 3,
            class : 'text-center',
            // "visible" : false                
            },{
            "targets": 4,
            class : 'text-center',
            // "visible" : false                
            },{
            "targets": 5,
            class : 'text-center',
            // "visible" : false                
            },{
            "targets": 6,
            // "data": '',
            render:function(data,type,row,meta){
                return '<button class="btn btn-info btn-sm" id="detailJual" data-toggle="modal" data-target="#editJual">Detail</button> '+
                      ' <button class="btn btn-danger btn-sm" id="deleteJual" >Hapus</button>';  
              }                
            }

            ]            

          }
        );

        $('#tableGps tbody').on('click','#detailJual', function(){
          $('#idJual').val('')
          $('#idGpsEdit').val('')
          $('#tglJualEdit').val('')
          $('#namaJualEdit').val('')
          $('#checkEdit').prop('checked',false);
          $('#btnEdit').hide();
          var data = table.row( $(this).parents('tr') ).data();
            $('#idJual').val(data.idJual).prop('disabled',true);
            $('#idGpsEdit').val(data.idGps).prop('disabled',true);
            $('#tglJualEdit').val(data.tglJualGps).prop('disabled',true);
            $('#namaJualEdit').val(data.jualKepada).prop('disabled',true);
            $('#checkEdit').prop('checked',false);
              $('#btnEdit').hide();
            
          $('#checkEdit').on('click', function(){
            if($(this).is(":checked")){
              $('#idJual').val(data.idJual).prop('disabled',false);
              $('#idGpsEdit').val(data.idGps).prop('disabled',false);
              $('#tglJualEdit').val(data.tglJualGps).prop('disabled',false);
              $('#namaJualEdit').val(data.jualKepada).prop('disabled',false);
              $('#checkEdit').prop('checked',true);
              $('#btnEdit').show();
            }
            else if($(this).is(":not(:checked)")){
              $('#idJual').val(data.idJual).prop('disabled',true);
              $('#idGpsEdit').val(data.idGps).prop('disabled',true);
              $('#tglJualEdit').val(data.tglJualGps).prop('disabled',true);
              $('#namaJualEdit').val(data.jualKepada).prop('disabled',true);
              $('#checkEdit').prop('checked',false);
              $('#btnEdit').hide();
            }
          });

        });
        
        $('#tableGps tbody').on('click','#deleteJual', function(){
          var data = table.row( $(this).parents('tr') ).data();

          url = '/admin/deleteJual/'+data.idJual;
          window.location = url;
        });

      });
    </script>
  </body>
</html>