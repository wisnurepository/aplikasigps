<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <title>Aplikasi Data GPS</title>
  </head>
  <body>

    <div class="container mt-5">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="/">Aplikasi Data GPS</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="/admin/dashboard">Dashboard <span class="sr-only">(current)</span></a>
          </li>          
          <li class="nav-item ">
            <a class="nav-link" href="/admin/pageUser">Manage User </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Manage GPS
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/admin/pageGps">GPS List</a>
              <a class="dropdown-item" href="/admin/pageJual">GPS Sold</a>
            </div>
          </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <a href="{{route('logout')}}" class="btn btn-danger"> Logout</a>
            {{-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
          </form>
        </div>
      </nav>
      <div class="jumbotron jumbotron-fluid p-0 mt-2">
        @if (session('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            GPS List
            <button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#createGpsNew">Add GPS</button>
          </div>
          <div class="card-body">
            <div class="container">
              <table class="table" id="tableGps">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">iDgps</th>
                    <th scope="col">garansiGps</th>
                    <th scope="col">linkGps</th>
                    <th scope="col">No</th>
                    <th scope="col">Brand GPS</th>
                    <th scope="col">Model GPS</th>
                    <th scope="col">GPS Name</th>
                    <th scope="col">Date GPS</th>
                    <th scope="col">deskripsi</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Modal Create GPS Here --}}
    <div class="modal fade bd-example-modal-xl" id="createGpsNew" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" style="min-width:58%" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Add New GPS</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>  
          <div class="modal-body">
            <form method="post" action="/admin/postGps" enctype="multipart/form-data">
              @csrf
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="">Brand GPS</label>
                  <input type="text" class="form-control" name="brand_gps" placeholder="Brand GPS" id="" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="">Model GPS</label>
                  <input type="text" class="form-control" name="model_gps" placeholder="Model GPS" id="" required>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="">Name GPS</label>
                  <input type="text" class="form-control" name="name_gps" placeholder="Name GPS" id="" required>
                </div>
                <div class="form-group col-md-4">
                  <label for="">Garansi GPS / Tahun</label>
                  <input type="number" class="form-control" name="garansi_gps" placeholder="Garansi GPS" id="" required>
                </div>
                <div class="form-group col-md-4">
                  <label for="">Date Buy GPS</label>
                  <input type="date" class="form-control" id="" name="date_buy" required>
                </div>
              </div>
              <div class="form-group">
                <label for="">Description GPS</label>
                <textarea name="deskripsi_gps" class="form-control" id="" cols="30" rows="10"></textarea>
              </div>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <div class="custom-file mt-2">
                    <input type="file" class="float-right" name="filePhoto" required>
                  </div>
                </div>

              </div>
              <button type="submit" class="btn btn-primary float-right">Add GPS</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    
    {{-- Modal Edit GPS Here --}}
    <div class="modal fade bd-example-modal-xl" id="editGps" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" style="min-width:58%" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Add New GPS</h5>
            
            <div class="form-group float-right">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="checkEdit">
                <label class="form-check-label" for="checkEdit">
                  Edit Data GPS
                </label>
              </div>
            </div>

          </div>  
          <div class="modal-body">
            <form method="post" action="/admin/editGps" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="idGps" id="idGpsEdit">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="">Brand GPS</label>
                  <input type="text" class="form-control" name="brand_gps_edit" placeholder="Brand GPS" id="brandgps" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="">Model GPS</label>
                  <input type="text" class="form-control" name="model_gps_edit" placeholder="Model GPS" id="modelgps" required>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="">Name GPS</label>
                  <input type="text" class="form-control" name="name_gps_edit" placeholder="Name GPS" id="namegps" required>
                </div>
                <div class="form-group col-md-4">
                  <label for="">Garansi GPS / Tahun</label>
                  <input type="number" class="form-control" name="garansi_gps_edit" placeholder="Garansi GPS" id="garansigps" required>
                </div>
                <div class="form-group col-md-4">
                  <label for="">Date Buy GPS</label>
                  <input type="date" class="form-control" id="dategps" name="date_buy_edit" required>
                </div>
              </div>
              {{-- <div class="form-group">
                <label for="">Description GPS</label>
                <textarea name="dekripsi_edit" class="form-control" id="deskripsigps" cols="30" rows="10"></textarea>
              </div> --}}
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="">Description GPS</label>
                  <textarea name="dekripsi_gps_edit" class="form-control" id="deskripsigps" cols="30" rows="10"></textarea>
                  </div>
                  <div class="form-group col-md-6">
                    <div id="imagesGps" class="mt-5">
                    </div>
                    <div class="custom-file mt-2">
                      <input type="file" class="form-control" name="filePhoto_edit">
                    </div>
                  </div>
                </div>

              </div>
              <button type="submit" class="btn btn-primary float-right" id="btnEdit">Edit GPS</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    

    <script>
      $(document).ready( function () {
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('#tableGps').DataTable(
                      {
                        "ordering": false,
                        "info":     false,
                        "bFilter": false,
                        "bLengthChange": false,
                        "processing": true,

                        ajax : {
                          url : "/admin/getTableList",
                          type : "GET",
                        },
                        
                        "columns" : [
                          {'data' : 'idGps'},
                          {'data' : 'garansiGps'},
                          {'data' : 'photoGps'},
                          {'data' : 'no'},
                          {'data' : 'brandGps'},
                          {'data' : 'modelGps'},
                          {'data' : 'namaGps'},
                          {'data' : 'tglBeliGps'},
                          {'data' : 'deskripsiGps'},
                        ],
                        
                        "columnDefs" : [{
                          "targets": 0,
                          class : 'text-center',
                          "visible" : false                
                          },{
                          "targets": 1,
                          class : 'text-center',
                          "visible" : false                
                          },{
                          "targets": 2,
                          class : 'text-center',
                          "visible" : false                
                          },{
                          "targets": 3,
                          class : 'text-center',
                          //"visible" : false                
                          },{
                          "targets": 4,
                          class : 'text-center',
                          //"visible" : false                
                          },{
                          "targets": 5,
                          class : 'text-center',
                          //"visible" : false                
                          },{
                          "targets": 6,
                          class : 'text-center',
                          //"visible" : false                
                          },{
                          "targets": 7,
                          class : 'text-center',
                          //"visible" : false                
                          },{
                          "targets": 8,
                          class : 'text-center',
                          "visible" : false                
                          },{
                          "targets": 9,
                          // "data": '',
                          render:function(data,type,row,meta){
                              return '<button class="btn btn-info btn-sm" id="detailGps" data-toggle="modal" data-target="#editGps">Detail</button> '+
                                    ' <button class="btn btn-danger btn-sm" id="deleteGps" >Hapus</button>';  
                            }                
                          }

                        ]

                      }
                    );

        $('#tableGps tbody').on('click','#detailGps', function(){
          $('#idGpsEdit').val('')
          $('#brandgps').val('').prop('disabled', true);
          $('#modelgps').val('').prop('disabled', true);
          $('#namegps').val('').prop('disabled', true);
          $('#garansigps').val('').prop('disabled', true);
          $('#dategps').val('').prop('disabled', true);
          $('#deskripsigps').val('').prop('disabled', true);
          $('#imagesGps').html('')
          $('#checkEdit').prop('checked',false);
          $('#btnEdit').hide();

          var data = table.row( $(this).parents('tr') ).data();
          // console.log(data);
              $('#idGpsEdit').val(data.idGps);
              $('#brandgps').val(data.brandGps).prop('disabled', true);
              $('#modelgps').val(data.modelGps).prop('disabled', true);
              $('#namegps').val(data.namaGps).prop('disabled', true);
              $('#garansigps').val(data.garansiGps).prop('disabled', true);
              $('#dategps').val(data.tglBeliGps).prop('disabled', true);
              $('#deskripsigps').val(data.deskripsiGps).prop('disabled', true);
              $('#imagesGps').append('<img style="width:300px; height:250px;" src="{{asset("/storage")}}/'+data.photoGps +'" class="img-responsive">');
          // console.log(data)
          $('#checkEdit').on('click', function(){
            if($(this).is(":checked")){
              $('#brandgps').val(data.brandGps).prop('disabled', false);
              $('#modelgps').val(data.modelGps).prop('disabled', false);
              $('#namegps').val(data.namaGps).prop('disabled', false);
              $('#garansigps').val(data.garansiGps).prop('disabled', false);
              $('#dategps').val(data.tglBeliGps).prop('disabled', false);
              $('#deskripsigps').val(data.deskripsiGps).prop('disabled', false);
              $('#checkEdit').prop('checked',true);
              $('#btnEdit').show();
            }
            else if($(this).is(":not(:checked)")){
              $('#brandgps').val(data.brandGps).prop('disabled', true);
              $('#modelgps').val(data.modelGps).prop('disabled', true);
              $('#namegps').val(data.namaGps).prop('disabled', true);
              $('#garansigps').val(data.garansiGps).prop('disabled', true);
              $('#dategps').val(data.tglBeliGps).prop('disabled', true);
              $('#deskripsigps').val(data.deskripsiGps).prop('disabled', true);
              $('#checkEdit').prop('checked',false);
              $('#btnEdit').hide();
            }
          });

        });

        $('#tableGps tbody').on('click', '#deleteGps', function(){
          var data = table.row( $(this).parents('tr') ).data();
          // console.log(data.idGps);

          url = '/admin/hapusGps/'+data.idGps;

          window.location = url;
        });
      });
    </script>
  </body>
</html>