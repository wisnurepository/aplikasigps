<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <title>Aplikasi Data GPS</title>
  </head>
  <body>

    <div class="container mt-5">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="/">Aplikasi Data GPS</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="/admin/dashboard">Dashboard <span class="sr-only">(current)</span></a>
          </li>          
          <li class="nav-item ">
            <a class="nav-link" href="">Manage User </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Manage GPS
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/admin/pageGps">GPS List</a>
              <a class="dropdown-item" href="/admin/pageJual">GPS Sold</a>
            </div>
          </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <a href="{{route('logout')}}" class="btn btn-danger"> Logout</a>
            {{-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
          </form>
        </div>
      </nav>
      <div class="jumbotron jumbotron-fluid p-0 mt-2">
        @if (session('success'))
        <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('edit'))
        <div class="col-sm-12">
            <div class="alert  alert-primary alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Update</span> {{session('edit')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @elseif (session('delete'))
        <div class="col-sm-12">
            <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            User List
            <button class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#tambahUser">Tambah User</button>
          </div>
          <div class="card-body">
            <div class="container">
              <table class="table" id="tableUser">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">idUser</th>
                    <th scope="col">No</th>
                    <th scope="col">Nama User</th>
                    <th scope="col">E-Mail</th>
                    <th scope="col">Role</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Modal User Create --}}
    <div class="modal fade bd-example-modal-xl" id="tambahUser" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Tambah User Baru</h5>
          </div>
          <div class="modal-body">
            <form method='post' action='/admin/tambahUser'>
              @csrf
              <div class="form-group">
                <label for="">Nama</label>
                <input type="text" class="form-control" id="" name="nama" placeholder="Nama" required>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="">Email</label>
                  <input type="email" class="form-control" id="" name="email" placeholder="Email" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="">Password</label>
                  <input type="password" class="form-control" id=""  name="password"placeholder="Password" required>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="">Role</label>
                  <select id="" class="form-control" name="role" required>
                    <option selected>-- Pilih Role --</option>
                    <option value="1">Admin</option>
                    <option value="2">User</option>
                  </select>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Tambah</button>
          </form>
          </div>
        </div>
      </div>
    </div>

    
    {{-- Modal User Create --}}
    <div class="modal fade bd-example-modal-xl" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Edit User Baru</h5>
            <div class="form-group float-right">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="checkEdit">
                <label class="form-check-label" for="checkEdit">
                  Edit Data GPS
                </label>
              </div>
            </div>
          </div>
          <div class="modal-body">
            <form method='post' action='/admin/editUser'>
              @csrf
              <input type="hidden" name="idUser" id="idUser">
              <div class="form-group">
                <label for="">Nama</label>
                <input type="text" class="form-control" id="editName" name="nama" placeholder="Nama" required>
              </div>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="">Email</label>
                  <input type="email" class="form-control" id="editEmail" name="email" placeholder="Email" required>
                </div>
                {{-- <div class="form-group col-md-6">
                  <label for="">Password</label>
                  <input type="password" class="form-control" id=""  name="password"placeholder="Password" required>
                </div> --}}
              </div>
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="">Role</label>
                  <select class="form-control" id="editRole" name="role" required>
                    <option selected>-- Pilih Role --</option>
                    <option value="1">Admin</option>
                    <option value="2">User</option>
                  </select>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="btnEdit">Edit User</button>
          </form>
          </div>
        </div>
      </div>
    </div>

    
    {{-- Modal User Create --}}
    <div class="modal fade bd-example-modal-xl" id="editPassword" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          {{-- <div class="modal-header">
            <h5>Ganti Passwords</h5>
          </div> --}}
          <div class="modal-body">
            <form method='post' action='/admin/editPassword'>
              @csrf
              <input type="hidden" name="idUserPass" id="idUserPass">
              <div class="form-group">
                <label for="">Password Baru</label>
                <input type="password" class="form-control" id="" name="password" placeholder="Password Baru" required>
              </div>
              <button type="submit" class="btn btn-primary float-right" id="btnEdit">Ubah Password</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    

    <script>
      $(document).ready( function () {
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('#tableUser').DataTable(
                      {
                        "ordering": false,
                        "info":     false,
                        "bFilter": false,
                        "bLengthChange": false,
                        "processing": true,

                        ajax : {
                          url : "/admin/getTableUser",
                          type : "GET",
                        },
                        
                        "columns" : [
                          {'data' : 'idUser'},
                          {'data' : 'no'},
                          {'data' : 'nameUser'},
                          {'data' : 'emailUser'},
                          {'data' : 'roleUser'},
                        ],
                        
                        "columnDefs" : [{
                          "targets": 0,
                          class : 'text-center',
                          "visible" : false                
                          },{
                          "targets": 1,
                          class : 'text-center',
                          // "visible" : false                
                          },{
                          "targets": 2,
                          class : 'text-center',
                          // "visible" : false                
                          },{
                          "targets": 3,
                          class : 'text-center',
                          //"visible" : false                
                          },{
                          "targets": 4,
                          class : 'text-center',
                          //"visible" : false     
                            render : function(data, type, row, meta) {
                                  if (row.roleUser == 1) {
                                    return "Admin";
                                  }
                                  else if (row.roleUser == 2) {
                                    return "User";
                                  }
                            }            
                          },{
                          "targets": 5,
                          // "data": '',
                          render:function(data,type,row,meta){
                              return '<button class="btn btn-info btn-sm" id="detailUser" data-toggle="modal" data-target="#editUser">Detail</button> '+
                                     '<button class="btn btn-warning btn-sm" id="gantiPassword" data-toggle="modal" data-target="#editPassword">Password</button> '+
                                     ' <button class="btn btn-danger btn-sm" id="deleteUser" >Hapus</button>';  
                            }                
                          }

                        ]

                      }
                    );

        $('#tableUser tbody').on('click','#detailUser', function(){
          $('#idUser').val('');
          $('#editName').val('');
          $('#editEmail').val('');
          $('#editRole').val('');
          $('#checkEdit').prop('checked',false);
          $('#btnEdit').hide();
          var data = table.row( $(this).parents('tr') ).data();
          $('#idUser').val(data.idUser).prop('disabled',true);
          $('#editName').val(data.nameUser).prop('disabled',true);
          $('#editEmail').val(data.emailUser).prop('disabled',true);
          $('#editRole').val(data.roleUser).prop('disabled',true);
          $('#checkEdit').prop('checked',false);
          $('#btnEdit').hide();
          $('#checkEdit').on('click', function(){
            if($(this).is(":checked")){
              $('#idUser').val(data.idUser).prop('disabled',false);
              $('#editName').val(data.nameUser).prop('disabled',false);
              $('#editEmail').val(data.emailUser).prop('disabled',false);
              $('#editRole').val(data.roleUser).prop('disabled',false);
              $('#checkEdit').prop('checked',true);
              $('#btnEdit').show();
            }
            else if($(this).is(":not(:checked)")){
              $('#idUser').val(data.idUser).prop('disabled',true);
              $('#editName').val(data.nameUser).prop('disabled',true);
              $('#editEmail').val(data.emailUser).prop('disabled',true);
              $('#editRole').val(data.roleUser).prop('disabled',true);
              $('#checkEdit').prop('checked',false);
              $('#btnEdit').hide();
            }
          });
        });

        $('#tableUser tbody').on('click','#gantiPassword', function(){
          $('#idUserPass').val('');
          var data = table.row( $(this).parents('tr') ).data();
          $('#idUserPass').val(data.idUser);

        })

        $('#tableUser tbody').on('click','#deleteUser', function(){
          var data = table.row( $(this).parents('tr') ).data();
            
            url = '/admin/deleteUser/'+data.idUser;
            window.location = url;
        });

      });
    </script>
  </body>
</html>