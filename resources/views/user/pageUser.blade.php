<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Aplikasi Data GPS</title>
  </head>
  <body>

    <div class="container mt-5">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="/">Aplikasi Data GPS</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="/user/dashboard">Dashboard <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="/user/pageUser">Profile </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Manage GPS
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="/user/pageGps">GPS List</a>
                <a class="dropdown-item" href="/user/pageJual">GPS Jual</a>
              </div>
            </li>
            {{-- <li class="nav-item">
              <a class="nav-link disabled" href="#">Disabled</a>
            </li> --}}
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <a href="{{route('logout')}}" class="btn btn-danger"> Logout</a>
            {{-- <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
          </form>
        </div>
      </nav>
      @if (session('success'))
      <div class="col-sm-12">
          <div class="alert  alert-success alert-dismissible fade show" role="alert">
              <span class="badge badge-pill badge-success">Success</span> {{session('success')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      </div>
      @elseif (session('edit'))
      <div class="col-sm-12">
          <div class="alert  alert-primary alert-dismissible fade show" role="alert">
              <span class="badge badge-pill badge-success">Update</span> {{session('update')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      </div>
      @elseif (session('delete'))
      <div class="col-sm-12">
          <div class="alert  alert-danger alert-dismissible fade show" role="alert">
              <span class="badge badge-pill badge-danger">Delete</span> {{session('delete')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
      </div>
      @endif
      <div class="jumbotron jumbotron-fluid mt-3">
        <div class="container">
          <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>
                              Profile
                              <div class="form-group float-right">
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" id="checkEdit">
                                  <label class="form-check-label" for="checkEdit">
                                    Edit Profile
                                  </label>
                                </div>
                              </div>
                            </h5>
                            <br>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="/user/userUpdate">
                              @csrf
                              <input type="hidden" name="iduser" id="iduser" value="{{Auth::user()->id}}">
                              <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">First Name</label> 
                                <div class="col-8">
                                  <input id="name" name="name" placeholder="First Name" class="form-control here" value="{{Auth::user()->name}}" type="text" >
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-4 col-form-label">Email*</label> 
                                <div class="col-8">
                                  <input id="email" name="email" placeholder="Email" class="form-control here" value="{{Auth::user()->email}}" required="required" type="text" >
                                </div>
                              </div>
                              <div class="form-group row">
                                <div class="offset-6 col-6">
                                  <button type="submit" id="btnPass" class="btn btn-primary float-right">Ubah Profile</button>
                                </div>
                              </div>
                            </form>
                        </div>
                    </div>
                    <button type="" id="btnEdit" class="btn btn-warning float-right" data-toggle="modal" data-target="#editPassword">Ubah Password</button>
                    
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>

    
    <div class="modal fade bd-example-modal-xl" id="editPassword" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <form method='post' action='/user/userPassword'>
              @csrf
              <input type="hidden" value="{{Auth::user()->id}}"  name="idUserPass" id="idUserPass">
              <div class="form-group">
                <label for="">Password Baru</label>
                <input type="password" class="form-control" id="" name="passwordUser" placeholder="Password Baru" required>
              </div>
              <button type="submit" class="btn btn-primary float-right" id="btnEdit">Ubah Password</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
      $(document).ready( function () {
        
            $('#checkEdit').prop('checked',false);
            $('#username').prop('disabled',true)
            $('#name').prop('disabled',true)
            $('#email').prop('disabled',true)
            $('#btnEdit').hide();
            $('#btnPass').hide();
        $('#checkEdit').on('click', function(){
          if($(this).is(":checked")){
            $('#username').prop('disabled',false)
            $('#name').prop('disabled',false)
            $('#email').prop('disabled',false)
            $('#checkEdit').prop('checked',true);
            $('#btnEdit').show();
            $('#btnPass').show();
          }
          else if($(this).is(":not(:checked)")){
            $('#username').prop('disabled',true)
            $('#name').prop('disabled',true)
            $('#email').prop('disabled',true)
            $('#checkEdit').prop('checked',false);
            $('#btnEdit').hide();
            $('#btnPass').hide();
          }
        });
      });      
    </script>
  </body>
</html>