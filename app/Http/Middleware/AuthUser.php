<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Session;

class AuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (Auth::check() && Auth::user()->role == 2) {
        //  User
        return $next($request);
      }
      
      else if (Auth::check() && Auth::user()->role == 1) {
        // Admin
        return redirect('/admin/dashboard');
      }
      else
      {
        Session::flush();
        return redirect('/');
      }
    }
}
