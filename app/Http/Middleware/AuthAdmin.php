<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Session;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (Auth::check() && Auth::user()->role == 1) {
          // Admin
          return $next($request);
      }
      else if (Auth::check() && Auth::user()->role == 2) {
          //  User
          return redirect('/user/dashboard');
      }
      else
      {
        Session::flush();
        return redirect('/');
      }
    }
}
