<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      if (Auth::check() && Auth::user()->role == 1) {
          // Admin
          return redirect(route('admin.dashboard'));
      }
      else if (Auth::check() && Auth::user()->role == 2) {
          //  User
          return redirect(route('user.dashboard'));
      }
      return $next($request);
    }
}
