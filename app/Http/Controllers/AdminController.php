<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use Carbon\Carbon;
use App\Datagps;
use App\Datajual;
use Storage;

class AdminController extends Controller
{
  public function __construct()
  {
    // $this->middleware('auth');
    $this->middleware('authadmin');
  }

  public function index()
  {
    return view('admin.dashboard');
  }

  public function pageGps()
  {
    return view('admin.pageGps');
  }

  public function pageJual()
  {
    $getGps = Datagps::get(['datagps.nama_gps','datagps.id']);
    return view('admin.pageSoldGps', compact('getGps'));
  }
  
  public function pageUser()
  {
    return view('admin.pageManageUser');
  }


  public function postGps(Request $request)
  {
    $createGps = new Datagps;
    $createGps->brand_gps = $request->brand_gps;
    $createGps->model_gps = $request->model_gps;
    $createGps->nama_gps = $request->name_gps;
    $createGps->garansi_gps = $request->garansi_gps;
    $createGps->tgl_beli_gps = $request->date_buy;
    $createGps->desciption_gps = $request->deskripsi_gps;
    
    $gambarpath = 'GPS/' . Carbon::now()->format('d-m-Y H-i-s') . '/Images';
    $dataImage = $this->Imageupload('Images' ,   $request->filePhoto, $gambarpath);
    $createGps->photo_gps = $dataImage;

    $createGps->save();

    return redirect()->back()->with('success','Berhasil menambahakn MODEL GPS.');
  }


  public function editGps(Request $request)
  {
    $key = Crypt::decryptString($request->idGps);

    $getGps = Datagps::where('id',$key)->first();

    $getGps->brand_gps = $request->brand_gps_edit;
    $getGps->model_gps = $request->model_gps_edit;
    $getGps->nama_gps = $request->name_gps_edit;
    $getGps->garansi_gps = $request->garansi_gps_edit;
    $getGps->tgl_beli_gps = $request->date_buy_edit;
    $getGps->desciption_gps = $request->dekripsi_gps_edit;
    if($request->filePhoto_edit)
    {
      Storage::delete($getGps->photo_gps);      
      $gambarpath = 'GPS/' . Carbon::now()->format('d-m-Y H-i-s') . '/Images';
      $dataImage = $this->Imageupload('Images' ,   $request->filePhoto_edit, $gambarpath);
      $getGps->photo_gps = $dataImage;
    }
    $getGps->update();

    return redirect()->back()->with('success','Update Brand GPS'. $request->brand_gps_edit .' berhasil');

  }

  public function hapusGps($id)
  {
    $key = Crypt::decryptString($id);
    $deleteGps = Datagps::where('id', $key)->first();
    Storage::delete($deleteGps->photo_gps);     
    $deleteGps->delete();

    return redirect()->back()->with('delete','Hapus data berhasil');
  }


  public function postJual(Request $request)
  {
    $postJual = new Datajual;
    $postJual->gps_id = $request->idGps;
    $postJual->tgl_jual_gps = $request->tglJual;
    $postJual->jual_kepada = $request->namaJual;
    $postJual->save();

    return redirect()->back()->with('success','Data Penjualan berhasil disimpan');
  }

  public function updateJual(Request $request)
  {
    $key = $request->key = Crypt::decryptString($request->idJual);

    $getData = Datajual::where('id',$key)->first();
    $getData->gps_id = $request->idGpsEdit;
    $getData->tgl_jual_gps = $request->tglJualEdit;
    $getData->jual_kepada = $request->namaJualEdit;
    $getData->update();
    
    
    return redirect()->back()->with('success','Data Penjualan berhasil diupdate');
  }

  public function deleteJual($id)
  {
    $key = Crypt::decryptString($id);
    $getData = Datajual::where('id',$key)->first();
    $getData->delete();
    
    return redirect()->back()->with('delete','Data Penjualan berhasil dihapus');
  }











  protected function Imageupload($column, $request, $store_path)
  {
    $file = $request;
    $filename = Carbon::now()->toDateString() . $column . '.' . $file->getClientOriginalExtension();
    $path = $file->storeAs($store_path, $filename, 'public');
    return $path;
  }

}
