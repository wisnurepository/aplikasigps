<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Datagps;
use App\Datajual;
Use App\User;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;

class DataTableController extends Controller
{

  public function getTableList()
  {

    $getData = Datagps::get();

    $data = array();
    $no = 1;

    foreach ($getData as $item)
    {
      $columns['idGps'] = (string)Crypt::encryptString($item->id);
      $columns['garansiGps'] = (string)($item->garansi_gps?$item->garansi_gps:'');
      $columns['photoGps'] = (string)($item->photo_gps?$item->photo_gps:'');
      $columns['no'] = (string)$no++;
      $columns['brandGps'] = (string)$item->brand_gps;
      $columns['modelGps'] = (string)$item->model_gps;
      $columns['namaGps'] = (string)$item->nama_gps;
      $columns['tglBeliGps'] = (string)$item->tgl_beli_gps;
      $columns['deskripsiGps'] = (string)$item->desciption_gps;
      $data[] = $columns;
    }

    
    if($data)
    {
      $response = [
                'data' => $data,
                'status' => 'Success Data',
                'kode' => '001',
              ];
    }
    elseif(!$data)
    {
      $response = [
                // 'data' => $data,
                'status' => 'Empty Data',
                'kode' => '002',
              ];

    }
    else
    {
      $response = [
                // 'data' => $data,
                'status' => 'Error Data',
                'kode' => '003',
              ];        
    }
    
    return response()->json($response);

  }

  public function getTableListJual()
  {
    $getData = Datajual::leftJoin('datagps','datagps.id','=','datajuals.gps_id')
                       ->get(
                         [
                           'datajuals.id',
                           'datajuals.gps_id',
                           'datagps.nama_gps',
                           'datajuals.tgl_jual_gps',
                           'datajuals.jual_kepada',                           
                         ]
                       );

    $data = array();
    $no = 1;

    foreach ($getData as $item)
    {
      $columns['no'] = (string)$no++;
      $columns['idJual'] = (string)Crypt::encryptString($item->id);
      $columns['idGps'] = (string)$item->gps_id;
      $columns['namaGps'] = (string)$item->nama_gps;
      $columns['tglJualGps'] = (string)$item->tgl_jual_gps;
      $columns['jualKepada'] = (string)$item->jual_kepada;

      $data[] = $columns;
    }

    
    if($data)
    {
      $response = [
                'data' => $data,
                'status' => 'Success Data',
                'kode' => '001',
              ];
    }
    elseif(!$data)
    {
      $response = [
                // 'data' => $data,
                'status' => 'Empty Data',
                'kode' => '002',
              ];

    }
    else
    {
      $response = [
                // 'data' => $data,
                'status' => 'Error Data',
                'kode' => '003',
              ];        
    }
    
    return response()->json($response);    
  }


  public function getTableUser()
  {
    $getData = User::all();

    $data = array();
    $no = 1;

    foreach ($getData as $item)
    {
      $columns['idUser'] = (string)Crypt::encryptString($item->id);
      $columns['no'] = (string)$no++;
      $columns['nameUser'] = (string)$item->name;
      $columns['emailUser'] = (string)$item->email;      
      $columns['roleUser'] = (string)$item->role;
      $data[] = $columns;
    }

    
    if($data)
    {
      $response = [
                'data' => $data,
                'status' => 'Success Data',
                'kode' => '001',
              ];
    }
    elseif(!$data)
    {
      $response = [
                // 'data' => $data,
                'status' => 'Empty Data',
                'kode' => '002',
              ];

    }
    else
    {
      $response = [
                // 'data' => $data,
                'status' => 'Error Data',
                'kode' => '003',
              ];        
    }
    
    return response()->json($response);    
  }

}
