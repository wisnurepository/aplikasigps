<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

use App\User;
use Auth;

class AuthController extends Controller
{
  public function tambahUser(Request $request)
  {
    User::create([
        'name' => $request->nama,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'role' => $request->role,
    ]);

    if($request->role == 1)
    {
      $data = 'Admin';
    }
    else
    {
      $data = 'User';
    }
    
    return redirect()->back()->with('success', 'Menambahkan '. $data .' berhasil');
  }

  public function editUser(Request $request)
  {
    $key = Crypt::decryptString($request->idUser);
    $getData = User::where('id', $key)->first();
    $getData->name = $request->nama;
    $getData->email = $request->email;
    $getData->role = $request->role;
    $getData->save();

    return redirect()->back()->with('success','Memperbarui user berhasil');
  }

  public function deleteUser($id)
  {
    $key = Crypt::decryptString($id);
    $getData = User::where('id', $key)->first();
    $getData->delete();
    
    return redirect()->back()->with('delete','Menghapus user berhasil');
  }

  public function editPassword(Request $request)
  {
    $key = Crypt::decryptString($request->idUserPass);
    $newPassword = Hash::make($request->password);
    $getData = User::where('id', $key)->first();
    $getData->password = $newPassword;
    $getData->update();
    
    return redirect()->back()->with('edit','Mengubah password user berhasil');
  }

  public function userUpdate(Request $request)
  {
    $getData = User::where('id',$request->iduser)->first();
    $getData->name = $request->name;
    $getData->email = $request->email;
    $getData->save();
    return redirect()->back()->with('success','Mengubah password user berhasil');
  }

  public function userPassword(Request $request)
  {
    $getData = User::where('id',$request->idUserPass)->first(); 
    $getData->password = Hash::make($request->passwordUser);
    $getData->save();
    
    Auth::logout();

    return redirect('/');
    
  }
}
